<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
	<jdoc:include type="head" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/styles.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/hover-min.css" type="text/css" />

	<script>
		$(document).ready(function(){
			$("#topmenu li").addClass("hvr-grow-rotate");
			$(".frame").addClass("hvr-push")
		});
	</script>
</head>
<body>

	<!-- topmenu -->	
	<?php if($this->countModules('topmenu')) : ?>

	<div id="topmenu">
		<div class="centeredframe">
			<jdoc:include type="modules" name="topmenu" style="xhtml" />
		</div>
	</div>
	<div style="overflow: auto; height: 50px;"></div>
	
	<?php endif; ?>
	
	<!-- search -->
	<?php if($this->countModules('search')) : ?>

	<div class="rowframe">
		<div class="centeredframe">
			<div class="textframe oybackground">
				<jdoc:include type="modules" name="search" style="xhtml" />
			</div>
		</div>
	</div>
	
	<?php endif; ?>
	
	<!-- top - 3 frames in a row -->
	<?php if($this->countModules('top1') or $this->countModules('top2') or $this->countModules('top3')) : ?>
	
	<div class="rowframe">
		<div class="centeredframenopadding">
			<div class="rbackground frame">
				<jdoc:include type="modules" name="top1" style="xhtml" />
			</div>
			<div class="obackground frame">
				<jdoc:include type="modules" name="top2" style="xhtml" />
			</div>
			<div class="ybackground frame">
				<jdoc:include type="modules" name="top3" style="xhtml" />
			</div>
		</div>
	</div>
	
	<?php endif; ?>

	<!-- content -->
	<div class="rowframe wbackground">
		<div class="centeredframe">
			<div class="textframe oybackground">
				<jdoc:include type="component" />
			</div>
		</div>
	</div>

	<!-- user -->
	<?php if($this->countModules('user')) : ?>
	
	<div class="rowframe bbackground">
		<div class="centeredframe">
			<div class="textframe oybackground">
				<jdoc:include type="modules" name="user" style="xhtml" />
			</div>
		</div>
	</div>

	<?php endif; ?>
	
	<!-- footer -->
	<?php if($this->countModules('footer')) : ?>
	
	<div class="rowframe bbackground" style="height: 300px;">
		<jdoc:include type="modules" name="footer" style="xhtml" />
	</div>

	<?php endif; ?>
	
</body>
</html>