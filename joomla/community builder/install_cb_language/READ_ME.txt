Install plug_language_english_en-GB_2.0.8.1.0.3+83c5d98a3.zip using CB Plugin Management page
Set en_GB language on top of the all language plugins
Install plug_cblanguageoverride.zip using CB Plugin Management page
Always keep CB Language Override plugin last in CB Plugin Management page list
Publish CB Language Override plugin
Copy component->com_comprofiler->plugin->user->plug_cblanguageoverride->language->default_language->language.php file to component->com_comprofiler->plugin->language->wx-yz->cbplugin->cblanguageoverride-language.php (replace wx-yz with your installed language, e.g. en-gb)
Copy component->com_comprofiler->plugin->user->plug_cblanguageoverride->language->default_language->admin_language.php file to component->com_comprofiler->plugin->language->wx-yz->cbplugin->cblanguageoverride-admin_language.php (replace wx-yz with your installed language, e.g. en-gb)
add language override definitions to your cblanguageoveride- files as needed.
See video #36 for example usage